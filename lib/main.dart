import 'package:appmobile/home.dart';
import 'package:appmobile/splash.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_core/firebase_core.dart';
import 'dart:async';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
       
        theme: ThemeData(
        
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        initialRoute: '/',
        routes: {
          '/': (context) => SplashPage(),
          // '/': (context) => SplashFuturePage(),
          '/Home': (context) => Home(),
        });
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.
  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  String dat = "";
  void _incrementCounter() {
    setState(() async {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
      dat = await updateData().toString();
      updateData();
    });
  }

  void createData() {
    final databaseReference = FirebaseDatabase.instance.ref();
    databaseReference
        .child("flutterDevsTeam2")
        .set({'name': 'nissaf', 'description': 'Team Lead'});
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // TRY THIS: Try changing the color here to a specific color (to
        // Colors.amber, perhaps?) and trigger a hot reload to see the AppBar
        // change color while the other colors stay the same.
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('nissssa    ' + '$dat' + 'your text'),
            FutureBuilder<String>(
              future: updateData(),
              builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return CircularProgressIndicator();
                } else if (snapshot.hasError) {
                  return Text('Error: ${snapshot.error}');
                } else {
                  final ref =
                      FirebaseDatabase.instance.ref().child('flutterDevsTeam3');
                  ref.onValue.listen((event) {
                    if (event.snapshot.exists) {
                      String name =
                          event.snapshot.child('name').value.toString();
                      // Update the UI with the new value
                      setState(() {
                        dat = name;
                      });
                    }
                  });
                  return Text(
                    snapshot.data ?? '',
                    style: Theme.of(context).textTheme.headlineMedium,
                  );
                }
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          print(dat);
          String data = await readData();
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Read Data'),
                content: Text(data),
                actions: <Widget>[
                  TextButton(
                    child: Text('Close'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
        },
        tooltip: 'Read Data',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

Future<String> readData() async {
  final ref = FirebaseDatabase.instance.ref();
  final snapshot = await ref.child('flutterDevsTeam3').get();
  if (snapshot.exists) {
    return snapshot.child('name').value.toString();
  } else {
    return "null";
  }
}

Future<String> updateData() {
  final completer = Completer<String>();
  final ref = FirebaseDatabase.instance.ref();
  final subscription = ref.child('flutterDevsTeam3').onValue.listen((event) {
    if (event.snapshot.exists) {
      String name = event.snapshot.child('name').value.toString();
      completer.complete('Updated value: $name');
    } else {
      completer.complete('null');
    }
  });
  // Wait for the completer to complete and then cancel the subscription
  completer.future.whenComplete(() => subscription.cancel());
  return completer.future;
}
