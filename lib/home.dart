import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'package:firebase_database/firebase_database.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final databaseReference = FirebaseDatabase.instance.ref();

  String temp = "";
  String hum = "";
  String light = "";
  String moteur_temp = "";
  String vibration = "";
  String small = "";
  String big = "";
  String total = "";

  double val = 0;
  double vibrat = 0;
  bool isAlertSet = false;
 
  @override
  Widget build(BuildContext context) {
    final ref = FirebaseDatabase.instance.ref();
    ref.onValue.listen((event) {
      if (event.snapshot.exists) {
        String valu_hum = event.snapshot.child('humidity').value.toString();
        String valu_temp = event.snapshot.child('temperature').value.toString();
        String valu_mot_temp =
            event.snapshot.child('temperature moteur').value.toString();
        String value_vib = event.snapshot.child('vibration').value.toString();
        String valu_light = event.snapshot.child('lumiere').value.toString();
        String value_small = event.snapshot.child('humidity').value.toString();
        String valu_big = event.snapshot.child('humidity').value.toString();
        String valu_total = event.snapshot.child('humidity').value.toString();

        // Update the UI with the new value
        setState(() {
          temp = valu_temp;
          hum = valu_hum;
          light = valu_light;
          moteur_temp = valu_mot_temp;
          vibration = value_vib;
          small = value_small;
          big = valu_big;
          total = valu_total;
          //val = double.parse(moteur_temp);
          vibrat = double.parse(vibration);
         val=  double.parse(event.snapshot.child('temperature moteur').value.toString());
          if (val >= 80 && isAlertSet == false) {
            showDialogBox();
            ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Attention hot tempeture')));
            setState(() => isAlertSet = true);
          }
        });
      }
    });
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          backgroundColor: Color.fromARGB(255, 52, 77, 82),
          title: const Text(
            'Egg Factory',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Color.fromARGB(255, 122, 145, 164),
                fontSize: 25),
          ),
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.add_alert),
              tooltip: 'Show Snackbar',
              color: const Color.fromARGB(255, 184, 203, 208),
              onPressed: () {
                ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Attention hot tempeture')));
              },
            ),
            IconButton(
                icon: const Icon(Icons.navigate_next),
                tooltip: 'Go to the next page',
                color: Color.fromARGB(255, 184, 203, 208),
                onPressed: () {})
          ]),
      body: Center(
        child: ListView(children: [
          Container(
            color: const Color.fromARGB(255, 112, 156, 167),
            padding: EdgeInsets.all(10),
            margin: const EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: Row(children: [
              Container(
                  child: Row(children: <Widget>[
                Image(
                  image: AssetImage(
                    "images/temperature.png",
                  ),
                  height: 45,
                  // width: 45,
                )
              ])),
              SizedBox(
                width: 55,
              ),
              Text("Temperature : ",
                  style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(
                width: 55,
              ),
              Text('$temp', style: TextStyle(fontWeight: FontWeight.bold)),
              Text(" °C ", style: TextStyle(fontWeight: FontWeight.bold)),
            ]),
          ),
          Container(
            color: const Color.fromARGB(255, 112, 156, 167),
            padding: EdgeInsets.all(10),
            margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
            child: Row(children: [
              Container(
                  child: Row(children: <Widget>[
                Image(
                  image: AssetImage(
                    "images/humidity.png",
                  ),
                  height: 45,
                  // width: 45,
                )
              ])),
              SizedBox(
                width: 55,
              ),
              Text("Humidity : ",
                  style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(
                width: 75,
              ),
              Text('$hum', style: TextStyle(fontWeight: FontWeight.bold)),
              Text(" % ", style: TextStyle(fontWeight: FontWeight.bold)),
            ]),
          ),
          Container(
            color: const Color.fromARGB(255, 112, 156, 167),
            padding: EdgeInsets.all(10),
            margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
            child: Row(children: [
              Container(
                  child: Row(children: <Widget>[
                Image(
                  image: AssetImage(
                    "images/lamp.png",
                  ),
                  height: 45,
                  // width: 45,
                )
              ])),
              SizedBox(
                width: 55,
              ),
              Text("Light        :",
                  style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(
                width: 75,
              ),
              Text('$light', style: TextStyle(fontWeight: FontWeight.bold)),
              Text(" lux ", style: TextStyle(fontWeight: FontWeight.bold)),
            ]),
          ),
          Container(
              padding: EdgeInsets.only(top: 16, left: 10),
              margin: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color.fromARGB(255, 19, 124, 139),
                  width: 3.0,
                ),
                color: Color.fromARGB(255, 184, 203, 208),
                borderRadius: BorderRadius.all(
                  const Radius.circular(20),
                ),
              ),
              child: Row(children: <Widget>[
                Container(
                    width: 150,
                    height: 180,
                    child: SfRadialGauge(
                        title: const GaugeTitle(
                            text: "Engine Temperature",
                            alignment: GaugeAlignment.center,
                            textStyle: TextStyle(
                              color: Color.fromARGB(255, 66, 65, 65),
                              fontSize: 15,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Roboto',
                            )),
                        axes: <RadialAxis>[
                          RadialAxis(
                              minimum: 0,
                              maximum: 50,
                              ranges: <GaugeRange>[
                                GaugeRange(
                                    startValue: 0,
                                    endValue: 10,
                                    color: Colors.green),
                                GaugeRange(
                                    startValue: 10,
                                    endValue: 30,
                                    color: Colors.orange),
                                GaugeRange(
                                    startValue: 30,
                                    endValue: 50,
                                    color: Colors.red)
                              ],
                              pointers: <GaugePointer>[
                                NeedlePointer(
                                  value: val,
                                  needleLength: 0.3,
                                  needleEndWidth: 4,
                                )
                              ],
                              annotations: <GaugeAnnotation>[
                                GaugeAnnotation(
                                    widget: Container(
                                        child: Text('$moteur_temp' + ' °C',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 10,
                                                fontWeight: FontWeight.bold))),
                                    angle: 85,
                                    positionFactor: 0.5)
                              ])
                        ])),
                SizedBox(
                  width: 3,
                ),
                Container(
                    width: 150,
                    height: 180,
                    child: SfRadialGauge(
                        title: const GaugeTitle(
                            text: "Engine Vibration",
                            alignment: GaugeAlignment.center,
                            textStyle: TextStyle(
                              color: Color.fromARGB(255, 66, 65, 65),
                              fontSize: 15,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Roboto',
                            )),
                        axes: <RadialAxis>[
                          RadialAxis(
                              minimum: 0,
                              maximum: 1000,
                              ranges: <GaugeRange>[
                                GaugeRange(
                                    startValue: 0,
                                    endValue: 300,
                                    color: Color.fromARGB(255, 12, 33, 168)),
                                GaugeRange(
                                    startValue: 300,
                                    endValue: 600,
                                    color: Color.fromARGB(255, 212, 182, 10)),
                                GaugeRange(
                                    startValue: 600,
                                    endValue: 1000,
                                    color: Color.fromARGB(255, 195, 73, 7))
                              ],
                              pointers: <GaugePointer>[
                                NeedlePointer(
                                  value: vibrat,
                                  needleLength: 0.3,
                                  needleEndWidth: 4,
                                )
                              ],
                              annotations: <GaugeAnnotation>[
                                GaugeAnnotation(
                                    widget: Container(
                                        child: Text('$vibration' + ' ',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 10,
                                                fontWeight: FontWeight.bold))),
                                    angle: 85,
                                    positionFactor: 0.5)
                              ])
                        ])),
                SizedBox(
                  width: 20,
                ),
              ])),
          Container(
            padding: EdgeInsets.only(left: 10),
            margin: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              border: Border.all(
                color: const Color.fromARGB(255, 19, 124, 139),
                width: 3.0,
              ),
              color: Color.fromARGB(255, 184, 203, 208),
              borderRadius: BorderRadius.all(
                const Radius.circular(20),
              ),
            ),
            child: Column(
              children: <Widget>[
                Row(children: <Widget>[
                  Container(
                      child: Row(
                    children: <Widget>[
                      Image(
                        image: AssetImage(
                          "images/open-box.png",
                        ),
                        height: 45,
                        // width: 45,
                      ),
                      Text(" small eggs : ",
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      Text(" 0 ",
                          style: TextStyle(fontWeight: FontWeight.bold)),
                    ],
                  )),
                  SizedBox(
                    width: 30,
                  ),
                  Container(
                      child: Row(
                    children: <Widget>[
                      Image(
                        image: AssetImage(
                          "images/open-box.png",
                        ),
                        height: 45,
                        // width: 45,
                      ),
                      Text(" big eggs :  ",
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      Text(" 0 ",
                          style: TextStyle(fontWeight: FontWeight.bold)),
                    ],
                  )),
                ]),
                Container(
                  margin: EdgeInsets.only(left: 105),
                  child: Row(
                    children: <Widget>[
                      Image(
                        image: AssetImage(
                          "images/the-sum-of.png",
                        ),
                        height: 55,
                        // width: 45,
                      ),
                      //SizedBox(width: 10,),
                      Text(" Total :",
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      Text(" 0 ",
                          style: TextStyle(fontWeight: FontWeight.bold)),
                    ],
                  ),
                )
              ],
            ),
          )
        ]),
      ),
      floatingActionButton: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //will break to another line on overflow
        // direction: Axis.horizontal, //use vertical to show  on vertical axis
        children: <Widget>[
          Container(
              margin: EdgeInsets.all(10),
              child: FloatingActionButton(
                onPressed: () {
                  //action code for button 1
                  createData('1');

              
                },
                backgroundColor: Color.fromARGB(255, 3, 105, 25),
                child: Icon(Icons.play_arrow),
              )), //button first

          Container(
              margin: EdgeInsets.all(10),
              child: FloatingActionButton(
                onPressed: () {
                  //action code for button 3
                  createData('2');
                },
                backgroundColor: Color.fromARGB(255, 218, 5, 5),
                child: Icon(Icons.stop),
              )), // button third

          // Add more buttons here
        ],
      ),
    );
  }

  showDialogBox() => showCupertinoDialog<String>(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: const Text('hot engine temperature '),
          content: const Text('active fan'),
          actions: <Widget>[
            TextButton(
              onPressed: ()  {
                setState(() => isAlertSet = false);
                 databaseReference.onValue.listen((event) {
                  if (event.snapshot.exists) {
                    val = double.parse(
                        event.snapshot.child('temperature moteur').value.toString());
                  }
                   if (val>=80 && isAlertSet == false) {
                  showDialogBox();
                  setState(() => isAlertSet = true);
                }
      
                  Navigator.pop(context, 'Cancel');

                  
                });
              },
              child: const Text('OK'),
            ),
          ],
        ),
      );
}

void createData(String a) {
  final databaseReference = FirebaseDatabase.instance.ref();
  databaseReference.child("moteur").set({'stat': a});
}

